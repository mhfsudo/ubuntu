#!/bin/bash

echo ""
echo "===================="
echo "Start of the Script"
echo "Good to see you"
echo ""

#cd /media/mhf/							# for second Drive with SynologyDrive
#sudo mkdir X							# for second Drive with SynologyDrive
#sudo mount /dev/nvme0n1p1 /media/mhf/X -o dmask=000,fmask=111	# for second Drive with SynologyDrive
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt-get autoclean
sudo apt-get autoremove
#sudo NSGClient -c                      # need for my Business laptop

echo ""
echo "End of the Script"
echo "===================="
echo ""
