#!/bin/bash

echo ""
echo "===================="
echo "Start of the Script"
echo ""
echo "Good, let us install some programs"
echo "first we install some required components and update the system"
echo "=========="

echo "=========="
sudo apt install curl
sudo apt install net-tools
sudo apt install snap
sudo apt install vim
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt-get autoclean
sudo apt-get autoremove

echo "=========="
echo "Anaconda for Python(y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	wget https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh
		bash Anaconda3-2019.03-Linux-x86_64.sh
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Atom Text Editor (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	firefox https://atom.io/download/deb
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Authy (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	{
			sudo apt update
			sudo apt install snapd
			sudo snap install authy --beta
		}
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Bitwarden Password Manager (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install bitwarden
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Brackets Text Editor (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install brackets --classic
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Brave Dev Browser(y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	{
			sudo apt install apt-transport-https curl
			curl -s https://brave-browser-apt-dev.s3.brave.com/brave-core-nightly.asc | sudo apt- key --keyring /etc/apt/trusted.gpg.d/brave-browser-prerelease.gpg add -
			echo "deb [arch=amd64] https://brave-browser-apt-dev.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-dev.list
			sudo apt update
			sudo apt install brave-browser-dev
		}
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Cheese Webcam (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt install cheese
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Chrome Dev Browser (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	curl -O https://www.google.com/chrome/dev
		bash dev
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Chromium (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install chromium
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Dconf Editor (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt-get install dconf-editor
		echo "=========="
		echo "Center Ubuntu Dock - org/gnome/shell/extensions/dash-to-dock (extend-height)"
		echo "Don't show mounted drives - org/gnome/shell/extensions/dash-to-dock (show-mounts)"
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Deja Dup Backup Tool (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt-get install deja-dup
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Docker (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt install docker.io
	       	docker version
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Drawing Tool (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo add-apt-repository ppa:cartes/drawing
	      	sudo apt update
	       	sudo apt upgrade
	       	sudo apt install drawing
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "FromScratch (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install fromscratch
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Java AdoptOpenJDK 11 mit OpenJ9 (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	        wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
	        sudo add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
	        sudo apt-get install adoptopenjdk-11-openj9
		java -version
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Eclipse IDE (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install --classic eclipse
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Flameshot Screenshot Tool (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt install flameshot
		echo "gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot"
		echo "Command: flameshot gui"
		echo "Shortcut: Print"
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Gimp (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt-get install gimp
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Git (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	    sudo apt-get install git
		echo ""
		echo "Git username?"
		read answer
		git config --global user.name "$answer"
		echo ""
		echo "Git mailaddress?"
		read answer
		git config --gloabl user.email "$answer"
		echo ""
		echo "Git Text Editor? (z.B. vim, nano, code, atom)"
		read answer
		git config --global core.editor "$answer"
		echo ""
		git config --global --list
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Gradle (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	curl -s "https://get.sdkman.io" | bash
          	source "$HOME/.sdkman/bin/sdkman-init.sh"
          	sdk version
          	sdk install gradle 6.7
          	gradle -v
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Groovy (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	url -s "https://get.sdkman.io" | bash
          	source "$HOME/.sdkman/bin/sdkman-init.sh"
          	sdk version
          	sdk install groovy
          	groovy -version
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "GitKraken (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install gitkraken --classic
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "JetBrains Tools (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	wget https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.18.7609.tar.gz
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Jupyter Notebook for Python (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt install python3-pip
		sudo pip install --upgrade pip
		sudo pip install jupyter
		sudo pip install notebook
		sudo apt-get install pandoc
            	sudo apt-get install texlive-xetex texlive-fonts-recommended
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "K8s Kubectl (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
		sudo snap install kubectl --classic
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "KeePass XC (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	{
			sudo add-apt-repository ppa:phoerious/keepassxc
			sudo apt update
			sudo apt install keepassxc
		}
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Libre Office (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
		sudo apt-get install libreoffice
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Maven (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
		sudo apt update
		sudo apt install maven
		mvn -version
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Microsoft Teams (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
		firefox https://go.microsoft.com/fwlink/p/?LinkID=2112886&clcid=0x807&culture=de-ch&country=CH
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "MySQL Workbench (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	{
			sudo add update
			sudo apt upgrade
			sudo apt install mysql-server
			sudo apt install mysql-workbench
			firefox https://linuxhint.com/install_mysql_workbench_ubuntu/
		}
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Noson Sonos Controller (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install noson
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Notepad++ Text Editor(y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install notepad-plus-plus
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "OnlyOffice (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	snap install onlyoffice-desktopeditors
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Opera Dev Browser(y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install opera-developer
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "ProtonMail Bridge (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	firefox https://beta.protonmail.com/settings/u/0/bridge#protonmail-bridge
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Razer (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	    	sudo add-apt-repository ppa:openrazer/stable
		sudo add-apt-repository ppa:polychromatic/stable
		sudo apt update
		sudo apt install openrazer-meta polychromatic
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Spotify (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install spotify
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Statup USB Creator (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt install usb-creator-gtk
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Synology Drive (y/n)"
read answer
case "$answer" in
  y) echo "wird installiert"
          	firefox https://www.synology.com/en-us/support/download/DS218play#utilities
    ;;
  n) echo "wird nicht installiert"
    ;;
esac

echo "=========="
echo "TexMaker (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt-get install texmaker
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Thunderbird Mail (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt-get install thunderbird
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Tweaks (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt-get install gnome-tweaks
		sudo apt install gnome-shell-extensions
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Virtual Box (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt install virtualbox
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Visual Studio Code IDE (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install code --classic
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Vivaldi Browser (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	wget -qO- http://repo.vivaldi.com/stable/linux_signing_key.pub | sudo apt-key add -
		sudo add-apt-repository "deb [arch=i386,amd64] http://repo.vivaldi.com/stable/deb/ stable main"
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "VLC Player (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt install vlc
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Whatsapp for Linux (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo snap install whatsdesk
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Wireshark (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	sudo apt install wireshark
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Zoom (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	firefox https://zoom.us/support/download
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt-get autoclean
sudo apt-get autoremove

echo "=========="
echo "Fish und Starship Shell Erweiterung (y/n)"
read answer
case "$answer" in
	y) echo "wird installiert"
	       	{
			sudo apt-add-repository ppa:fish-shell/release-3
			sudo apt update
			#Fish und powerline (https://github.com/powerline/fonts)
			sudo apt install fish fonts-powerline
			#Fischer (https://github.com/jorgebucaran/fisher)
			curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish
			#Starship
			curl -fsSL https://starship.rs/install.sh | bash
			echo "starship init fish | source" > .config/fish/config.fish
			chsh -s `which fish`
			fish
			set fish_greeting
		}
		;;
	n) echo "wird nicht installiert"
		;;
esac

echo "=========="
echo "Puuhh, that was a lot of work"
echo "Should we reboot the System?"
read answer
case "$answer" in
  y) echo "wird neugestartet"
         sudo reboot
    ;;
  n) echo "wird nicht neugestartet"
    ;;
esac

echo ""
echo "End of the Script"
echo "===================="
echo ""
echo ""
