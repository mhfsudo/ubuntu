#!/bin/bash

echo ""
echo "===================="
echo "Start of the Script"
echo "Good, let's update your system"
echo ""

sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt-get autoclean
sudo apt-get autoremove

echo ""
echo "End of the Script"
echo "===================="
echo ""
