#!/bin/bash

echo ""
echo "===================="
echo "Start of the Script"
echo "Good, see you soon"
echo ""

#bash umount.sh			        # for second Drive with SynologyDrive
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt-get autoclean
sudo apt-get autoremove
#sudo pkill NSGClient           # need for my Business laptop
sudo systemctl poweroff		

echo ""
echo "End ot the Script"
echo "===================="
echo ""
