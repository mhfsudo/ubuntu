#!/bin/bash

echo ""
echo "===================="
echo "Run the Container"
echo ""

browse  http://localhost:3000

sourcedir=$HOME/source
docker run -it -p 3000:3000 -v "$sourcedir:/home/project:cached" theiaide/theia
i
