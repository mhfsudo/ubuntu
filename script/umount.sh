#!/bin/bash

echo ""
echo "===================="
echo "Start of the Script"
echo "Good, let's umount the X Drive"
echo ""

sudo umount /dev/nvme0n1p1
cd /media/mhf/
sudo rmdir X

echo ""
echo "End of the Script"
echo "===================="
echo ""
